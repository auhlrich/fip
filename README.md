# Flight-In-Portugal

## Setup
docker-compose.yml contains configuration to start a mysql container for save history of requests.

- Run docker-compose up -d to start container
- Run FlightInPortugalApplication.java
 

## How it works
The API provides the GET /flight/avg wich calls an external api to get flight information and calculates the average prices.
We only accept destinations to LIS (Lisbon) or OPO (Porto). The avarege is only for the companies TAP anr Ryanair.
Every call is persisted in a database, that can be called at 
GET /history. You can clean this history with a DELETE /hi  story call

This API uses SWAGGER for documentation. 
http://localhost:8080/swagger-ui.html
                  