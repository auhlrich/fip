package com.fip.flightinportugal;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.annotation.EnableCaching;

@SpringBootApplication
@EnableCaching
public class FlightInPortugalApplication {

	public static void main(String[] args) {
		SpringApplication.run(FlightInPortugalApplication.class, args);
	}

}
