package com.fip.flightinportugal.api.controllers;

import com.fip.flightinportugal.api.models.dtos.FlightInformationRequestDTO;
import com.fip.flightinportugal.api.models.dtos.FlightInformationResponseDTO;
import com.fip.flightinportugal.api.services.FlightInformationService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import lombok.RequiredArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.time.LocalDate;

@RestController
@RequiredArgsConstructor
@RequestMapping(
        value = "flight",
        produces = MediaType.APPLICATION_JSON_VALUE
)
@ApiResponses(value = {
        @ApiResponse(code = 400, message = "Bad request. Parameters with errors or violating bussines rules")
})
@Api(  description ="Search flight information from LIS->OPO or OPO->LIS")

public class FlightInformationController {

    private final FlightInformationService flightInformationService;

    @GetMapping(value = "/avg")
    public ResponseEntity<FlightInformationResponseDTO> getFlightsAveragePrice(
            @ApiParam(value = "Destination Ariports. Only LIS or OPO options are accepted.", example = "LIS")
            @RequestParam(value = "dest", required = true) String destination,

            @ApiParam(value = "Date From. Use yyyy/MM/dd format. Can't be in the past", example = "2020/12/25")
            @RequestParam(value = "dateFrom", required = false) @DateTimeFormat(pattern = "yyyy/MM/dd") LocalDate dateFrom,

            @ApiParam(value = "Date To. Use yyyy/MM/dd format. Can't be before dateFrom parameter", example = "2020/12/25")
            @RequestParam(value = "dateTo", required = false) @DateTimeFormat(pattern = "yyyy/MM/dd") LocalDate dateTo,

            @ApiParam(value = "Currency", example = "USD")
            @RequestParam(value = "curr", required = false) String currrency
    ) {
        FlightInformationRequestDTO flightInformationRequestDTO = new FlightInformationRequestDTO(destination, dateFrom, dateTo, currrency);
        return ResponseEntity.ok(flightInformationService.getFlightsAveragePrice(flightInformationRequestDTO));
    }
}

