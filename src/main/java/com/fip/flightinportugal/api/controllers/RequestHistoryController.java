package com.fip.flightinportugal.api.controllers;


import com.fip.flightinportugal.api.models.RequestsHistory;
import com.fip.flightinportugal.api.services.RequestsHistoryService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping(
        value = "history",
        produces = MediaType.APPLICATION_JSON_VALUE
)
@Api(  description ="API to list request history and clean it")

public class RequestHistoryController {

    private final RequestsHistoryService requestsHistoryService;

    @ApiOperation(value = "List all history records")
    @GetMapping()
    ResponseEntity<List<RequestsHistory>> listAll() {
        return ResponseEntity.ok(requestsHistoryService.findAll());
    }

    @ApiOperation(value = "Delete all history records")
    @DeleteMapping()
    public ResponseEntity<Void> deleteAll() {
        requestsHistoryService.deleteAll();
        return ResponseEntity.noContent().build();
    }

}
