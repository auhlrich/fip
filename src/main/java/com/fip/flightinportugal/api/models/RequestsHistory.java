package com.fip.flightinportugal.api.models;

import com.fip.flightinportugal.api.models.dtos.FlightInformationRequestDTO;
import com.fip.flightinportugal.api.models.dtos.FlightInformationResponseDTO;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import java.time.LocalDate;
import java.time.LocalDateTime;

@Entity
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Data
public class RequestsHistory {

    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    private Long id;

    private LocalDateTime requestDateTime;
    private String requestURI;

    private String destination;
    private LocalDate dateFrom;
    private LocalDate dateTo;
    private String currrency;

    private Double priceAverage;
    private Double bag1Average;
    private Double bag2Average;
}
