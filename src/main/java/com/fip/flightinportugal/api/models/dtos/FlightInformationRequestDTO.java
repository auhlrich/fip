package com.fip.flightinportugal.api.models.dtos;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDate;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class FlightInformationRequestDTO {
    private String destination;
    private LocalDate dateFrom;
    private LocalDate dateTo;
    private String currrency;
}
