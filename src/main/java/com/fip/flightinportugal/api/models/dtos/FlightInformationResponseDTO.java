package com.fip.flightinportugal.api.models.dtos;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDate;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class FlightInformationResponseDTO {
    private String currency;
    private LocalDate dateFrom;
    private LocalDate dateTo;
    private Double priceAverage;
    private BagsAverageDTO bagsPrice;

}
