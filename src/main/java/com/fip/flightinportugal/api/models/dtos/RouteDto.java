package com.fip.flightinportugal.api.models.dtos;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

@Data
@JsonIgnoreProperties(value = {"found_on", "source"})
public class RouteDto implements Serializable {

    @JsonProperty("fare_basis")
    private String fareBasis;

    @JsonProperty("fare_category")
    private String fareCategory;

    @JsonProperty("fare_classes")
    private String fareClasses;

    @JsonProperty("fare_family")
    private String fareFamily;

    @JsonProperty("bags_recheck_required")
    private Boolean bagsRecheckRequired;

    @JsonProperty("aTimeUTC")
    private Date aTimeUTC;

    private String mapIdfrom;

    private String mapIdto;

    @JsonProperty("flight_no")
    private Integer flightNo;

    @JsonProperty("operating_carrier")
    private String operatingCarrier;

    @JsonProperty("dTime")
    private Date dTime;

    private BigDecimal latTo;

    private String flyTo;

    @JsonProperty("return")
    private Integer returnField;

    private String id;

    @JsonProperty("combination_id")
    private String combinationId;

    @JsonProperty("original_return")
    private Integer originalReturn;

    private String airline;

    private BigDecimal lngTo;

    private String cityTo;

    private String cityFrom;

    private String cityCodeFrom;

    private String cityCodeTo;

    private BigDecimal lngFrom;

    @JsonProperty("aTime")
    private Date aTime;

    private String flyFrom;

    private BigDecimal price;

    private BigDecimal latFrom;

    @JsonProperty("dTimeUTC")
    private Date dTimeUTC;

    @JsonProperty("last_seen")
    private Date lastSeen;

    @JsonProperty("refresh_timestamp")
    private Date refreshTimestamp;

    private Boolean guarantee;

    private String equipment;

    @JsonProperty("vehicle_type")
    private String vehicleType;

    @JsonProperty("operating_flight_no")
    private String operatingFlightNo;
}
