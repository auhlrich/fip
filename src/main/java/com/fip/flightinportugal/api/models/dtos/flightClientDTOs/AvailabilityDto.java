package com.fip.flightinportugal.api.models.dtos.flightClientDTOs;

import lombok.Data;

import java.io.Serializable;

@Data
public class AvailabilityDto implements Serializable {

    private Integer seats;
}
