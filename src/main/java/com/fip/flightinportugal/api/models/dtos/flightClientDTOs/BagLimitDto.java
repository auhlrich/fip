package com.fip.flightinportugal.api.models.dtos.flightClientDTOs;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;

@Data
public class BagLimitDto implements Serializable {

    @JsonProperty("hand_height")
    private BigDecimal handHeight;

    @JsonProperty("hand_length")
    private BigDecimal handLength;

    @JsonProperty("hand_weight")
    private BigDecimal handWeight;

    @JsonProperty("hand_width")
    private BigDecimal handWidth;

    @JsonProperty("hold_height")
    private BigDecimal holdHeight;

    @JsonProperty("hold_length")
    private BigDecimal holdLength;

    @JsonProperty("hold_weight")
    private BigDecimal holdWeight;

    @JsonProperty("hold_width")
    private BigDecimal holdWidth;

    @JsonProperty("hold_dimensions_sum")
    private BigDecimal holdDimensionsSum;

}
