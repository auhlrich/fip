package com.fip.flightinportugal.api.models.dtos.flightClientDTOs;


import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;

@Data
public class BagsPriceDto implements Serializable {

    @JsonProperty("1")
    private BigDecimal one;

    @JsonProperty("2")
    private BigDecimal two;

    private BigDecimal hand;
}