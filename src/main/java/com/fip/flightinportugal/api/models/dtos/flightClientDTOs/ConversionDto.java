package com.fip.flightinportugal.api.models.dtos.flightClientDTOs;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;

@Data
public class ConversionDto implements Serializable {

    private BigDecimal foreignPrice;

    private String foreignCurrencyCode;

    @JsonProperty("EUR")
    private BigDecimal euroPrice;
}