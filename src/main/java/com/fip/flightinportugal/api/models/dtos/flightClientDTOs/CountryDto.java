package com.fip.flightinportugal.api.models.dtos.flightClientDTOs;

import lombok.Data;

import java.io.Serializable;

@Data
public class CountryDto implements Serializable {
    private String code;
    private String name;
}
