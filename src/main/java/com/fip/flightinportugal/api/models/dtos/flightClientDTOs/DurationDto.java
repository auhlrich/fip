package com.fip.flightinportugal.api.models.dtos.flightClientDTOs;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.io.Serializable;

@Data
public class DurationDto implements Serializable {

    private Long departure;

    @JsonProperty("return")
    private Long returnField;

    private Long total;
}
