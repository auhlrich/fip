package com.fip.flightinportugal.api.models.dtos.flightClientDTOs;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;

@Data
@JsonIgnoreProperties(value = {"refresh", "ref_tasks", "all_stopover_airports", "all_airlines"})
public class ResponseDTO implements Serializable {

    @JsonProperty("search_id")
    private String searchId;

    @JsonProperty("search_params")
    private SearchParamsDto searchParamsDto;

    private Integer time;

    private List<String> connections;

    private String currency;

    @JsonProperty("currency_rate")
    private BigDecimal currencyRate;

    private List<DataDto> data;

    @JsonProperty("fx_rate")
    private BigDecimal fxRate;

    private BigDecimal del;

}
