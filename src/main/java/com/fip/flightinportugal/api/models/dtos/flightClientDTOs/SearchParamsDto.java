package com.fip.flightinportugal.api.models.dtos.flightClientDTOs;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fip.flightinportugal.api.models.dtos.Seats;
import lombok.Data;

import java.io.Serializable;

@Data
public class SearchParamsDto implements Serializable {
    @JsonProperty("to_type")
    private String toType;

    @JsonProperty("flyFrom_type")
    private String flyFromType;

    private Seats seats;

}
