package com.fip.flightinportugal.api.repositories;

import com.fip.flightinportugal.api.models.RequestsHistory;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface RequestsHistoryRepository extends JpaRepository<RequestsHistory, Long> {
}

