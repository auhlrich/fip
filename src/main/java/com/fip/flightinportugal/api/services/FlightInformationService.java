package com.fip.flightinportugal.api.services;

import com.fip.flightinportugal.api.models.dtos.FlightInformationRequestDTO;
import com.fip.flightinportugal.api.models.dtos.FlightInformationResponseDTO;

public interface FlightInformationService {
    FlightInformationResponseDTO getFlightsAveragePrice(FlightInformationRequestDTO flightInformationRequestDTO);
}
