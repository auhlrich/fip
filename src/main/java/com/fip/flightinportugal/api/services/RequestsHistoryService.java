package com.fip.flightinportugal.api.services;

import com.fip.flightinportugal.api.models.RequestsHistory;

import java.util.List;

public interface RequestsHistoryService {
    List<RequestsHistory> findAll();
    void deleteAll();
}
