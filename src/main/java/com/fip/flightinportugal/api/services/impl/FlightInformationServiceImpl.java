package com.fip.flightinportugal.api.services.impl;

import com.fip.flightinportugal.api.exeptions.BussinessException;
import com.fip.flightinportugal.api.models.RequestsHistory;
import com.fip.flightinportugal.api.models.dtos.BagsAverageDTO;
import com.fip.flightinportugal.api.models.dtos.FlightInformationRequestDTO;
import com.fip.flightinportugal.api.models.dtos.FlightInformationResponseDTO;
import com.fip.flightinportugal.api.models.dtos.flightClientDTOs.DataDto;
import com.fip.flightinportugal.api.models.dtos.flightClientDTOs.ResponseDTO;
import com.fip.flightinportugal.api.repositories.RequestsHistoryRepository;
import com.fip.flightinportugal.api.services.FlightInformationService;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Arrays;
import java.util.List;

@Service
@RequiredArgsConstructor
public class FlightInformationServiceImpl implements FlightInformationService {

    @Value(value = "${default.destinations}")
    private String defaultDestinations;

    @Value(value = "${default.currency}")
    private String defaultCurrency;

    @Value(value = "${flight.api.url}")
    private String flightAPIURL;

    @Value(value = "${flight.api.params}")
    private String flightAPIParams;
    private int i = 1;

    private final RequestsHistoryRepository requestsHistoryRepository;

    @Override
    @Cacheable("flight")
    public FlightInformationResponseDTO getFlightsAveragePrice(FlightInformationRequestDTO flightInformationRequestDTO) {
        validateRequest(flightInformationRequestDTO);
        ResponseEntity<ResponseDTO> result = callFlightAPI(flightInformationRequestDTO);

        FlightInformationResponseDTO flightInformationResponseDTO = calculateAverage(result.getBody());
        flightInformationResponseDTO.setCurrency(flightInformationRequestDTO.getCurrrency());
        flightInformationResponseDTO.setDateFrom(flightInformationRequestDTO.getDateFrom());
        flightInformationResponseDTO.setDateTo(flightInformationRequestDTO.getDateTo());

        savehistory(flightInformationRequestDTO, flightInformationResponseDTO);

        return flightInformationResponseDTO;
    }


    private void savehistory(FlightInformationRequestDTO flightInformationRequestDTO, FlightInformationResponseDTO flightInformationResponseDTO) {
        requestsHistoryRepository.save(buildRequestHistory(flightInformationRequestDTO, flightInformationResponseDTO));
    }

    private RequestsHistory buildRequestHistory(FlightInformationRequestDTO flightInformationRequestDTO, FlightInformationResponseDTO flightInformationResponseDTO) {
        return RequestsHistory.builder()
                .requestDateTime(LocalDateTime.now())
                .requestURI(getQuery(flightInformationRequestDTO))
                .currrency(flightInformationRequestDTO.getCurrrency())
                .dateFrom(flightInformationRequestDTO.getDateFrom())
                .dateTo(flightInformationRequestDTO.getDateTo())
                .destination(flightInformationRequestDTO.getDestination())
                .priceAverage(flightInformationResponseDTO.getPriceAverage())
                .bag1Average(flightInformationResponseDTO.getBagsPrice().getBag1Average())
                .bag2Average(flightInformationResponseDTO.getBagsPrice().getBag2Average())
                .build();
    }

    private FlightInformationResponseDTO calculateAverage(ResponseDTO responseDTO) {
        List<DataDto> dataDtoList = responseDTO.getData();
        return FlightInformationResponseDTO.builder()
                .bagsPrice(BagsAverageDTO.builder()
                        .bag1Average(getAverageBag1(dataDtoList))
                        .bag2Average(getAverageBag2(dataDtoList))
                        .build())
                .priceAverage(getAveragePrice(dataDtoList))
                .build();
    }


    private double getAveragePrice(List<DataDto> dataDtoList) {
        return dataDtoList.stream()
                .filter(value -> value.getPrice() != null)
                .mapToDouble(value -> value.getPrice().doubleValue())
                .average().getAsDouble();
    }

    private double getAverageBag1(List<DataDto> dataDtoList) {
        return dataDtoList.stream()
                .filter(value -> value.getBagsPrice().getOne() != null)
                .mapToDouble(value -> value.getBagsPrice().getOne().doubleValue())
                .average().getAsDouble();
    }

    private double getAverageBag2(List<DataDto> dataDtoList) {
        return dataDtoList.stream()
                .filter(value -> value.getBagsPrice().getTwo() != null)
                .mapToDouble(value -> value.getBagsPrice().getTwo().doubleValue())
                .average().getAsDouble();
    }

    private ResponseEntity<ResponseDTO> callFlightAPI(FlightInformationRequestDTO flightInformationRequestDTO) {
        return new RestTemplate().exchange(
                getQuery(flightInformationRequestDTO),
                HttpMethod.GET,
                getHttpEntity(),
                ResponseDTO.class
        );
    }

    private HttpEntity<?> getHttpEntity() {
        HttpHeaders headers = new HttpHeaders();
        return new HttpEntity<>(headers);
    }

    private String getQuery(FlightInformationRequestDTO flightInformationRequestDTO) {
        var dateTimeFormatter = DateTimeFormatter.ofPattern("dd/MM/yyyy");
        return UriComponentsBuilder
                .fromHttpUrl(flightAPIURL)
                .queryParam("flyFrom", (flightInformationRequestDTO.getDestination().equals("LIS")) ? "OPO" : "LIS")
                .queryParam("to", flightInformationRequestDTO.getDestination())
                .queryParam("dateFrom", dateTimeFormatter.format(flightInformationRequestDTO.getDateFrom()))
                .queryParam("dateTo", dateTimeFormatter.format(flightInformationRequestDTO.getDateTo()))
                .queryParam("curr", flightInformationRequestDTO.getCurrrency())
                .query(flightAPIParams).toUriString();
    }

    private void validateRequest(FlightInformationRequestDTO flightInformationRequestDTO) {
        validateDestination(flightInformationRequestDTO.getDestination());
        validateDates(flightInformationRequestDTO.getDateFrom(), flightInformationRequestDTO.getDateTo());
    }

    private void validateDates(LocalDate dateFrom, LocalDate dateTo) {
        validateDateCannotBeNull(dateFrom);
        validateDateCannotBeNull(dateTo);
        validateDateToIsAfterFrom(dateFrom, dateTo);
        validateDateIsFuture(dateFrom);
        validateDateIsFuture(dateTo);
    }

    private void validateDateIsFuture(LocalDate date) {
        if (date.isBefore(LocalDate.now())) {
            throw new BussinessException("Date cannot be in the past");
        }
    }

    private void validateDateCannotBeNull(LocalDate date) {
        if (date == null) {
            throw new BussinessException("Date cannot be null");
        }
    }

    private void validateDateToIsAfterFrom(LocalDate dateFrom, LocalDate dateTo) {
        if (dateFrom.isAfter(dateTo)) {
            throw new BussinessException("Parameter dateFrom must be before parameter dateTo");
        }
    }

    private void validateDestination(String destination) {
        Arrays.stream(defaultDestinations.split(","))
                .filter(value -> value.equals(destination))
                .findFirst()
                .orElseThrow(() -> new BussinessException(String.format("Destination must be one of this values:%s", defaultDestinations)));
    }

}
