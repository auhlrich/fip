package com.fip.flightinportugal.api.services.impl;

import com.fip.flightinportugal.api.models.RequestsHistory;
import com.fip.flightinportugal.api.repositories.RequestsHistoryRepository;
import com.fip.flightinportugal.api.services.RequestsHistoryService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class RequestsHistoryServiceImpl implements RequestsHistoryService {

    private final RequestsHistoryRepository requestsHistoryRepository;

    @Override
    public List<RequestsHistory> findAll() {
        return requestsHistoryRepository.findAll();
    }

    @Override
    public void deleteAll() {
        requestsHistoryRepository.deleteAll();
    }
}
