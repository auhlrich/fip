package com.fip.flightinportugal.api.controllers;

import com.fip.flightinportugal.api.services.FlightInformationService;
import com.fip.flightinportugal.helpers.TestHelper;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(FlightInformationController.class)
@ActiveProfiles("test")
class FlightInformationControllerTest {

    @MockBean
    private FlightInformationService flightInformationService;

    @Autowired
    MockMvc mvc;

    @Test
    void getFlightsAveragePriceTest() throws Exception {
        mvc.perform(get(TestHelper.getRequestUri("EUR", TestHelper.getFormatedFutureDateFrom(), TestHelper.getFormatedFutureDateTo(),"OPO"))
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());
        verify(flightInformationService, times(1)).getFlightsAveragePrice(any());


    }

}