package com.fip.flightinportugal.api.controllers;

import com.fip.flightinportugal.api.services.RequestsHistoryService;
import com.fip.flightinportugal.helpers.TestHelper;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;

import static org.hamcrest.Matchers.hasSize;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(RequestHistoryController.class)
@ActiveProfiles("test")
class RequestHistoryControllerTest {

    @MockBean
    private RequestsHistoryService requestsHistoryService;

    @Autowired
    MockMvc mvc;

    @Test
    void listAllTest() throws Exception {
        when(requestsHistoryService.findAll()).thenReturn(TestHelper.getListOfHistory());
        ResultActions resultActions = mvc.perform(get("/history")
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(2)));
    }

    @Test
    void deleteAllTest() throws Exception {
        ResultActions resultActions = mvc.perform(delete("/history")
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isNoContent());
        verify(requestsHistoryService, times(1)).deleteAll();
    }


}