package com.fip.flightinportugal.api.services;

import com.fip.flightinportugal.api.exeptions.BussinessException;
import com.fip.flightinportugal.api.models.dtos.FlightInformationRequestDTO;
import com.fip.flightinportugal.api.repositories.RequestsHistoryRepository;
import com.fip.flightinportugal.api.services.impl.FlightInformationServiceImpl;
import com.fip.flightinportugal.helpers.TestHelper;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.boot.test.mock.mockito.SpyBean;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;


@ExtendWith(SpringExtension.class)
@ActiveProfiles("test")
@TestPropertySource(properties = {
        "default.destinations=OPO,LIS",
        "default.currency=EUR",
        "flight.api.url=https://api.skypicker.com/flights",
        "flight.api.params=&partner=picky&v=3&select_airlines=FR,TP"
})
class FlightInformationServiceTest {

    @SpyBean
    FlightInformationServiceImpl flightInformationService;

    @MockBean
    RequestsHistoryRepository requestsHistoryRepository;

    @Test
    void getFlightsAveragePrice() {
        FlightInformationRequestDTO validFlightInformationRequestDTO = TestHelper.getValidFlightInformationRequestDTO();
        flightInformationService.getFlightsAveragePrice(validFlightInformationRequestDTO);
        verify(requestsHistoryRepository, times(1)).save(any());
    }

    @Test
    void shouldNotCallWithAnotherDestinationsThanLISOrOPO() {
        FlightInformationRequestDTO validFlightInformationRequestDTO = TestHelper.getDestinationInvalidFlightInformationRequestDTO();
        assertThrows(BussinessException.class,
                () -> flightInformationService.getFlightsAveragePrice(validFlightInformationRequestDTO));
    }

    @Test
    void shouldNotCallWithDatesInPast() {
        FlightInformationRequestDTO validFlightInformationRequestDTO = TestHelper.getDateInPastFlightInformationRequestDTO();
        assertThrows(BussinessException.class,
                () -> flightInformationService.getFlightsAveragePrice(validFlightInformationRequestDTO));
    }

    @Test
    void shouldNotCallWithDatesInLongFuture() {
        FlightInformationRequestDTO validFlightInformationRequestDTO = TestHelper.getDateInFarFutureFlightInformationRequestDTO();
        assertThrows(BussinessException.class,
                () -> flightInformationService.getFlightsAveragePrice(validFlightInformationRequestDTO));
    }

    @Test
    void shouldNotCallWithFromDateAfterToDate() {
        FlightInformationRequestDTO validFlightInformationRequestDTO = TestHelper.getDatesWithFromDateAfterToDateFlightInformationRequestDTO();
        assertThrows(BussinessException.class,
                () -> flightInformationService.getFlightsAveragePrice(validFlightInformationRequestDTO));
    }


}