package com.fip.flightinportugal.api.services;

import com.fip.flightinportugal.api.repositories.RequestsHistoryRepository;
import com.fip.flightinportugal.api.services.impl.RequestsHistoryServiceImpl;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.boot.test.mock.mockito.SpyBean;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;


@ExtendWith(SpringExtension.class)
@ActiveProfiles("test")
class RequestsHistoryServiceTest {

    @SpyBean
    RequestsHistoryServiceImpl  requestsHistoryService;

    @MockBean
    RequestsHistoryRepository requestsHistoryRepository;

    @Test
    void findAllTest() {
        requestsHistoryService.findAll();
        verify(requestsHistoryRepository, times(1)).findAll();
    }

    @Test
    void deleteAllTest() {
        requestsHistoryService.deleteAll();
        verify(requestsHistoryRepository, times(1)).deleteAll();
    }
}