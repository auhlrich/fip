package com.fip.flightinportugal.helpers;

import com.fip.flightinportugal.api.models.RequestsHistory;
import com.fip.flightinportugal.api.models.dtos.FlightInformationRequestDTO;
import lombok.experimental.UtilityClass;
import org.springframework.web.util.UriComponentsBuilder;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

@UtilityClass
public class TestHelper {

    public List<RequestsHistory> getListOfHistory() {
        List<RequestsHistory> requestsHistoryList = new ArrayList<RequestsHistory>();
        requestsHistoryList.add(getHistory(1));
        requestsHistoryList.add(getHistory(2));
        return requestsHistoryList;
    }

    public String getRequestUri(String currency, String dateFrom, String dateTo, String destination) {
        return UriComponentsBuilder.fromPath("/flight/avg")
                .queryParam("curr", currency)
                .queryParam("dateFrom", dateFrom)
                .queryParam("dateTo", dateTo)
                .queryParam("dest", destination)
                .toUriString();
    }

    public RequestsHistory getHistory(long id) {
        return RequestsHistory.builder().id(id)
                .destination("OPO")
                .requestDateTime(LocalDateTime.now())
                .priceAverage(10.0)
                .bag1Average(1.2)
                .bag2Average(2.2)
                .build();
    }

    public static String getFormatedFutureDateFrom() {
        var dateTimeFormatter = DateTimeFormatter.ofPattern("yyyy/MM/dd");
        return getFutureDateFrom().format(dateTimeFormatter);
    }


    public static String getFormatedFutureDateTo() {
        var dateTimeFormatter = DateTimeFormatter.ofPattern("yyyy/MM/dd");
        return getFutureDateTo().format(dateTimeFormatter);
    }


    public static String getFormatedPastDate() {
        var dateTimeFormatter = DateTimeFormatter.ofPattern("yyyy/MM/dd");
        return getPastDate().format(dateTimeFormatter);
    }

    private static LocalDateTime getPastDate() {
        return LocalDateTime.now().minusDays(3);
    }

    private static LocalDateTime getFutureDateFrom() {
        return LocalDateTime.now().plusDays(1);
    }

    private static LocalDateTime getFutureDateTo() {
        return LocalDateTime.now().plusDays(3);
    }

    private static LocalDateTime getFarFuture() {
        return LocalDateTime.now().plusYears(5);
    }


    public static FlightInformationRequestDTO getValidFlightInformationRequestDTO() {
        return FlightInformationRequestDTO.builder()
                .currrency("EUR")
                .destination("LIS")
                .dateFrom(getFutureDateFrom().toLocalDate())
                .dateTo(getFutureDateTo().toLocalDate())
                .build();
    }

    public static FlightInformationRequestDTO getDestinationInvalidFlightInformationRequestDTO() {
        return FlightInformationRequestDTO.builder()
                .currrency("EUR")
                .destination("GRU")
                .dateFrom(getFutureDateFrom().toLocalDate())
                .dateTo(getFutureDateTo().toLocalDate())
                .build();
    }

    public static FlightInformationRequestDTO getDateInPastFlightInformationRequestDTO() {
        return FlightInformationRequestDTO.builder()
                .currrency("EUR")
                .destination("GRU")
                .dateFrom(getPastDate().toLocalDate())
                .dateTo(getPastDate().toLocalDate())
                .build();
    }

    public static FlightInformationRequestDTO getDateInFarFutureFlightInformationRequestDTO() {
        return FlightInformationRequestDTO.builder()
                .currrency("EUR")
                .destination("GRU")
                .dateFrom(getFarFuture().toLocalDate())
                .dateTo(getFarFuture().toLocalDate())
                .build();
    }

    public static FlightInformationRequestDTO getDatesWithFromDateAfterToDateFlightInformationRequestDTO() {
        return FlightInformationRequestDTO.builder()
                .currrency("EUR")
                .destination("LIS")
                .dateTo(getFutureDateFrom().toLocalDate())
                .dateFrom(getFutureDateTo().toLocalDate())
                .build();
    }
}
